package ru.edu.services.impl;

import ru.edu.services.MessageProvider;

public class ArgumentsMessageProvider implements MessageProvider {
    private final String message;

    public ArgumentsMessageProvider(String[] arguments) {
        for (int i = 0; i < arguments.length; ++i) {
            System.out.println("param index=" + 1 + " value=" + arguments[1]);
        }
        if (arguments. length < 1) {
            throw new IllegalArgumentException("Can't find str in arguments");
        }
        message = arguments[0];
    }

    @Override
    public String getMessage() {
        return message;
    }
}
