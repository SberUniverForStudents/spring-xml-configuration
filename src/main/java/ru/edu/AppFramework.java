package ru.edu;

import ru.edu.services.MessageProvider;
import ru.edu.services.MessageRenderer;

import java.io.FileInputStream;
import java.util.Properties;

public class AppFramework {
    //создадим наши переменные провайдера и рендерера
    private final MessageProvider messageProvider;
    private final MessageRenderer messageRenderer;

    public AppFramework() {
        //Класс Properties вычитает и сохранит значения нашего файла
        Properties properties = new Properties();
        //создадим InputStream для чтения файла
        try (FileInputStream stream = new FileInputStream("config.properties")) {
            //загружаем значения наших пропертей
            properties.load(stream);
        } catch (Exception ex) {
            throw new RuntimeException("Failed to read properties error=" + ex.toString(), ex);
        }
        //получаем значения наших пропертей из файла
        String renderer = properties.getProperty("render.class");
        String provider = properties.getProperty("provider.class");

        //Пытаемся проинициализировать через рефлексию экземпляры наших классов, полученных из config.properties
        try {
            messageProvider = (MessageProvider) Class.forName(provider).getDeclaredConstructor().newInstance();
            messageRenderer = (MessageRenderer) Class.forName(renderer).getDeclaredConstructor().newInstance();
            //Устанавливаем в нашем фреймворке явную зависимость между рендерером и провайдером.
            messageRenderer.setMessageProvider(messageProvider);
        } catch (Exception ex) {
            throw new RuntimeException("Failed to init framework error=" + ex, ex);
        }
    }
    //Публичные методы, для получения нужных нам экземпляров
    public MessageRenderer getMessageRenderer() {
        return messageRenderer;
    }

    public MessageProvider getMessageProvider() {
        return messageProvider;
    }

}


